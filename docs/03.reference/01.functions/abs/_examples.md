The following code example will output some example numbers returned from the abs() function.

```lucee
<cfoutput>
	<ul>
		<li>1 = #abs(1)#</li>
		<li>1.1 = #abs(-1.1)#</li>
		<li>-1 = #abs(-1)#</li>
		<li>-1.1 = #abs(-1.1)#</li>
	</ul>
</cfoutput>
```